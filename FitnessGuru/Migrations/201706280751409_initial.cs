namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        FName = c.String(),
                        LName = c.String(),
                        Address = c.String(),
                        PhoneNumber = c.String(),
                        RegisteredDate = c.DateTime(nullable: true),
                        Gender = c.Int(nullable: true),
                        Birthday = c.DateTime(nullable: true),
                        Age = c.Int(nullable: true),
                        Height = c.Int(nullable: true),
                        CurrentWt = c.Int(nullable: true),
                        Goalwt = c.Int(nullable: true),
                        ActivenessId = c.Int(nullable: true),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserInfoes");
        }
    }
}
